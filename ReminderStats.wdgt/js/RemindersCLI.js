function RemindersCLI(){
    var self = this;

    // MARK: Private API
    var bin = '/usr/local/bin/reminders';
    var sys = {};
    
    var execute = function(command, options, onComplete){
        if( !window.widget ){ return; }
        var id = '_exec_' + Date.now();

        var options = options ? options : {};
        if( options['--format'] === undefined ) {
            options['--format'] = 'json';
        }
    
        var optionString = '';
        for( var key in options ) {
            var value = options[key];
    
            if( value !== undefined && value.length ){
                optionString += ' ' + key + '=' + value;
            } else {
                optionString += ' ' + key;
            }
        }
        
        var exec = bin.trim() + ' ' + command.trim() + (optionString.trim().length ? (' ' + optionString.trim()) : '');
        alert(id + ': '+ exec);
        
        sys[id] = widget.system(exec, function(){
            if( onComplete && typeof(onComplete) === 'function' ){
                onComplete.call(self, sys[id].outputString);
            }
            delete sys[id];
        });
    };


    // MARK: Public API

    self.ls = function(options, onComplete){
        execute('ls', options, onComplete);
    };

    self.lists = function(options, onComplete) {
        execute('list ls', options, onComplete);
    }
}