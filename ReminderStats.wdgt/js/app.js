var app = {
    api: null,
    _configCache: {},

    //
    // Function: onLoad()
    // Called by HTML body element's onload event when the widget is ready to start
    //
    onLoad: function(e){
        app.api = new RemindersCLI();

        // BACK
        document.getElementById('range').value = app.configGet('range', 'current-week');
        document.getElementById('lists').value = app.configGet('lists', '');
        document.getElementById('objective').value = app.configGet('objective', 100);

        setTimeout(function(){
            app.reload();
        }, 0);
    },

    reload: function(){
        if( !this.api ){ return; }
        var self = this;

        document.getElementsByTagName('h4')[0].textContent = app.configGet('objective', 100);
        document.getElementsByTagName('progress')[0].max = app.configGet('objective', 100);

        self.api.lists({}, function(out){
            if( !out ){ return };
            var json = JSON.parse(out);
            
            if( json.status === 'success' ){
                var select = document.getElementById('lists');

                while( select.children.length > 1 ){
                    var i = select.children.length - 1;
                    if( select.children[i].value !== '' ){
                        select.remove(i);
                    }
                }

                var configLists = app.configGet('lists', '');
                for( var i = 0; i < json.data.length; i++ ){
                    var list = json.data[i];

                    var option = document.createElement('option');
                    option.value = list.id;
                    option.selected = configLists.indexOf(list.id) != -1;
                    option.text = list.title;
                    
                    option.setAttribute('data-color', list.color);
                    select.appendChild(option);
                }
                app.tintColorSet(app.configGet('color'));
            }

            var titleElement = document.getElementsByTagName('h3')[0];
            titleElement.textContent = '';

            var rangeElement = document.getElementsByTagName('h2')[0];
            var locale = 'de-ch';

            var now = new Date();
            var opts = {};
            switch( app.configGet('range', 'current-week') ){
                case 'current-month':
                    var firstDayOfMonth = new Date(Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), 1));
                    opts['--completed-from'] = firstDayOfMonth.toISOString().split('T')[0];

                    rangeElement.textContent = firstDayOfMonth.toLocaleString(locale, { month: 'long' });
                    break;

                case 'last-month':
                    var year = now.getUTCFullYear();
                    var month = now.getUTCMonth() - 1;

                    if( month < 0 ){
                        month = 11;
                        year -= 1;
                    }
                    var firstDayOfMonth = new Date(Date.UTC(year, month, 1));
                    var lastDayOfMonth = new Date(Date.UTC(year, month + 1, 0));

                    opts['--completed-from'] = firstDayOfMonth.toISOString().split('T')[0];
                    opts['--completed-to'] = lastDayOfMonth.toISOString().split('T')[0];

                    rangeElement.textContent = firstDayOfMonth.toLocaleString(locale, { month: 'long' });
                    break;

                case 'current-year':
                    var firstDayOfYear = new Date(Date.UTC(now.getUTCFullYear(), 0, 1));
                    opts['--completed-from'] = firstDayOfYear.toISOString().split('T')[0];

                    rangeElement.textContent = firstDayOfYear.getFullYear();
                    break;

                case 'last-year':
                    var year = now.getUTCFullYear() - 1;
                    var firstDayOfYear = new Date(Date.UTC(year, 0, 1));
                    var lastDayOfYear = new Date(Date.UTC(year, 12, 0));

                    opts['--completed-from'] = firstDayOfYear.toISOString().split('T')[0];
                    opts['--completed-to'] = lastDayOfYear.toISOString().split('T')[0];

                    rangeElement.textContent = year;
                    break;

                case 'last-week':
                    var lastWeek = new Date(Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate() - 7));
                    
                    opts['--completed-from'] = lastWeek.firstDayOfWeek().toISOString().split('T')[0];
                    opts['--completed-to'] = lastWeek.lastDayOfWeek().toISOString().split('T')[0];
                    
                    rangeElement.textContent = 'KW ' + lastWeek.weekNumberOfYear();
                    break;

                default: //current-week
                    opts['--completed-from'] = now.firstDayOfWeek().toISOString().split('T')[0];
                    
                    rangeElement.textContent = 'KW ' + now.weekNumberOfYear();
                    break;
            }

            var term = app.configGet('term', '');
            if( term ){
                opts['--term'] = '"' + term + '"';
                titleElement.textContent = term;
            }

            var lists = app.configGet('lists', '');
            if( lists ) {
                opts['--lists'] = lists;

                if( !titleElement.textContent && lists.indexOf(',') == -1 ) {
                    titleElement.textContent = app.selectedOptionById('lists').text;
                }
            }

            self.api.ls(opts, function(out){
                if( !out ){ return; }
                var json = JSON.parse(out);

                if( json.status === 'success' ){
                    var points = json.data.reduce(function(sum, reminder){
                        switch( reminder.priority ){
                            case 1: return sum + 5; //high
                            case 5: return sum + 3; //medium
                            case 9: return sum + 1; //low
                            default: return sum;
                        }
                    }, 0);

                    document.getElementsByTagName('h1')[0].textContent = points;
                    document.getElementsByTagName('progress')[0].value = points;
                    document.getElementsByTagName('h5')[0].textContent = '' + Math.round(points * 100 / app.configGet('objective', 100)) + '%';
                }
            });
        });
    },

    //
    // Function: onRemove()
    // Called when the widget has been removed from the Dashboard
    //
    onRemove: function(e){
        // Stop any timers to prevent CPU usage
        // Remove any preferences as needed
        // widget.setPreferenceForKey(null, dashcode.createInstancePreferenceKey("your-key"));
    },

    //
    // Function: onHide()
    // Called when the widget has been hidden
    //
    onHide: function(e){
        // Stop any timers to prevent CPU usage
    },


    //
    // Function: onShow()
    // Called when the widget has been shown
    //
    onShow: function(e){
        setTimeout(function(){
            app.reload();
        },0);
    },

    //
    // Function: onSync()
    // Called when the widget has been synchronized with .Mac
    //
    onSync: function(e){
        // Retrieve any preference values that you need to be synchronized here
        // Use this for an instance key's value:
        // instancePreferenceValue = widget.preferenceForKey(null, dashcode.createInstancePreferenceKey("your-key"));
        //
        // Or this for global key's value:
        // globalPreferenceValue = widget.preferenceForKey(null, "your-key");
    },

    //
    // Function: showBack(event)
    // Called when the info button is clicked to show the back of the widget
    //
    // event: onClick event from the info button
    //
    showBack: function(e){
        var front = document.getElementById("front");
        var back = document.getElementById("back");

        if( window.widget ){
            widget.prepareForTransition("ToBack");
        }

        front.className = 'container hidden';
        back.className = 'container';

        setTimeout(function(){
            if( window.widget ){
                widget.performTransition();
            }
        }, 0);
    },

    //
    // Function: showFront(event)
    // Called when the done button is clicked from the back of the widget
    //
    // event: onClick event from the done button
    //
    showFront: function(e){
        var front = document.getElementById("front");
        var back = document.getElementById("back");

        if( window.widget ){
            widget.prepareForTransition("ToFront");
        }

        back.className = 'container hidden';
        front.className = 'container';

        setTimeout(function(){
            if( window.widget ){
                widget.performTransition();
            }
            document.getElementsByTagName('h2')[0].textContent = '';
            app.reload();
        }, 0);
    },

    configGet: function(key, defaultValue){
        var cachedValue = app._configCache[ key ];
        if( typeof(cachedValue) !== 'undefined' ) {
            return cachedValue;
        }

        if( window.widget ){
            var value = widget.preferenceForKey(widget.identifier + '-' + key);;
            if( typeof(value) === 'undefined' ){
                return defaultValue;
            }
            app._configCache[ key ] = value;
            return value;
        }
        return defaultValue;
    },

    configSet: function(key, value) {
        if( value instanceof Element ){
            switch( value.id ){
                case 'lists':
                    var lists = [];
                    var colors = [];

                    for( var i = 0; i < value.options.length; i++ ) {
                        if( value.options[i].selected ){
                            lists.push(value.options[i].value);
                            colors.push(value.options[i].getAttribute('data-color'));
                        }
                    }

                    app.configSet('lists', lists.join(','));
                    app.configSet('color', colors.length == 1 ? colors.join() : '');
                    break;
                
                default:
                    app.configSet(key, value.value);
                    break;
            }
            return;
        }

        if( window.widget ){
            widget.setPreferenceForKey(value, widget.identifier + '-' + key);
        }
        app._configCache[ key ] = value;
    },

    selectedOptionById: function(id) {
        var el = document.getElementById(id);

        if( el.selectedIndex == -1 ){
            return null;
        }
        return el.options[el.selectedIndex];
    },

    tintColorSet(color){
        if( !color ){
            color = app.configGet('color', '#007AFF');
        }

        var percent = document.getElementsByTagName('h5')[0];
        percent.style = 'color: ' + color;

        var list = document.getElementsByTagName('h3')[0];
        list.style = 'color: ' + color;

        var progressStyle = document.getElementById('progress-style');
        if( progressStyle ){
            progressStyle.remove();
        }
        progressStyle = document.createElement('style');
        progressStyle.id = 'progress-style';
        progressStyle.textContent = 'progress.tint-color{ background-color: ' + color + '; } progress.tint-color::-webkit-progress-value { background-color: ' + color + '; } progress.tint-color::-moz-progress-bar { background-color: ' + color + '; }';
        document.body.appendChild(progressStyle);
    }
};