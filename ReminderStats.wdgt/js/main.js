if( window.widget ){
    widget.onremove = app.onRemove;
    widget.onhide = app.onHide;
    widget.onshow = app.onShow;
    widget.onsync = app.onSync;
}